/**
 * 
 */
package mx.gob.sat.acuse.sellado.service;

import mx.gob.sat.exception.sellado.SelladoException;

/**
 * @author jbatis
 *
 */
public interface SelladorService {

	/**
	 * @param cadenaOriginal
	 * @return cadena sellada a patir de la cadena original.
	 * @throws SelladoException
	 */
	public String doSellado(String cadenaOriginal) throws SelladoException;
	
	/**
	 * Ruta para configurar el servicio de la selladora.
	 * @param pathSelladoraService
	 */
	public void setSelladoraService(String pathSelladoraService);
	
	/**
	 * Detalla el tipo de sellado
	 * @param tipoSellado
	 */
	public void setTipoSellado(int tipoSellado);
}
