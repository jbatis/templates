/**
 * 
 */
package mx.gob.sat.utils.acuse;

import java.util.Map;

import mx.gob.sat.exception.acuse.ExceptionAcuseConfig;

/**
 * @author BABJ827J
 */
public interface AcuseService {

	enum TIPO_ACUSE {
		SOLICITUD, RESOLUCION, BAJA
	}
	
	public void setNombreTemplate(String nombreTemplate);
	
	public Map<String, Object> doConfigParamsAcuse(TIPO_ACUSE tipoSolicitud) throws ExceptionAcuseConfig;
	
}
